/* gcc ext4-watcher.c -o ext4-watcher -I ~/var/mnt/buildripper/home/krisman/linux-build/ext4/usr/include */

#include <stdio.h>
#include<sys/ioctl.h>
#include <unistd.h>
#include <stdlib.h>
#include <linux/watch_queue.h>
#include <string.h>

static void consumer(int fd)
{
	unsigned char buffer[433], *p, *end;
	union {
		struct watch_notification n;
		unsigned char buf1[128];
		struct superblock_error_notification sen;
		struct superblock_inode_error_notification isen;
		struct superblock_msg_notification msg;
	} n;
	ssize_t buf_len;

	for (;;) {
		buf_len = read(fd, buffer, sizeof(buffer));
		if (buf_len == -1) {
			perror("read");
			exit(1);
		}

		if (buf_len == 0) {
			printf("-- END --\n");
			return;
		}

		if (buf_len > sizeof(buffer)) {
			fprintf(stderr, "Read buffer overrun: %zd\n", buf_len);
			return;
		}

		printf("read() = %zd\n", buf_len);

		p = buffer;
		end = buffer + buf_len;
		while (p < end) {
			size_t largest, len;

			largest = end - p;
			if (largest > 128)
				largest = 128;
			if (largest < sizeof(struct watch_notification)) {
				fprintf(stderr, "Short message header: %zu\n", largest);
				return;
			}
			memcpy(&n, p, largest);

			printf("NOTIFY[%03zx]: ty=%06x sy=%02x i=%08x\n",
			       p - buffer, n.n.type, n.n.subtype, n.n.info);

			len = n.n.info & WATCH_INFO_LENGTH;
			if (len < sizeof(n.n) || len > largest) {
				fprintf(stderr, "Bad message length: %zu/%zu\n", len, largest);
				exit(1);
			}

			switch (n.n.subtype) {
			case NOTIFY_SUPERBLOCK_ERROR:
				printf("\t SB AT %s:%d ERROR: %d\n", n.sen.function, n.sen.line,
				       n.sen.error_number);
				if (len > sizeof(n.sen))
					printf("description: %s\n", n.sen.desc);
				break;

			case NOTIFY_SUPERBLOCK_INODE_ERROR:
				printf("\t INODE[%llu] ERROR at %s:%d ERROR: %d\n", n.isen.inode,
				       n.isen.function, n.isen.line, n.isen.error_number);
				break;
			case NOTIFY_SUPERBLOCK_MSG:
				printf("\t Ext4 MSG: %s\n", n.msg.desc);
				break;
			default:
				printf("unknown  subtype %d\n");

			}

			p += len;
		}
	}
}

#define NR_WATCH_SB 440

int main ()
{
	int fd[2];
	char buffer[256];

	if (syscall(293, fd, O_NOTIFICATION_PIPE) < 0) {
		printf("pipe fail\n");
		return 1;
	}

	if (ioctl(fd[0], IOC_WATCH_QUEUE_SET_SIZE, 256) < 0) {
		printf("ioctl fail\n");
		return 1;
	}

	if (syscall(NR_WATCH_SB, 0, "/mnt", NULL, fd[0], 0x3) < 0)
		printf("watch sb failed\n");

	consumer (fd[0]);
}
